<div class="col-md-4">
   <div class="well">
      <h4>Pretraži blog</h4>
      <form action="search.php" method="post">
         <div class="input-group">
            <input type="text" name="search" class="form-control">
            <span class="input-group-btn">
            <button class="btn btn-search" name="submit" type="submit">
            <span class="glyphicon glyphicon-th"></span>
            </button>
            </span>
         </div>
      </form>
   </div>
   <div class="well">
      <h4>Log in</h4>
      <form action="includes/login.php" method="post">
         <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username" require>
         </div>
         <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" require> 
         </div>
         <input class="btn btn-primary" type="submit" name="login" value="Log in">  
      <form action="includes/login.php" method="post">
         <a href="registration.php">Sign up</a>
         <br>
         <br>
         <?php if(isset($_GET['loginError'])){ ?>
         <div class="alert alert-danger">
            <strong>Wrong username or password. Please try again!</strong>
         </div>
         <?php } ?>
         <?php if(isset($_GET['registerSuccess'])){ ?>
         <div class="alert alert-success">
            <strong>You have successfully registered! You can now sign up.</strong>
         </div>
         <?php } ?>
      </form>
   </div>
   <div class="well">
      <?php
         $query = "SELECT * FROM categories"; 
         $select_categories_sidebar = mysqli_query($connection,$query);
         ?> 
      <h4>Blog categories</h4>
      <div class="row">
         <div class="col-lg-12">
            <ul class="list-unstyled">
               <?php
                  while($row = mysqli_fetch_assoc($select_categories_sidebar)){
                  $cat_title = $row['cat_title'];
                  echo "<li> <a href='#'>{$cat_title}</a></li>"; 
                  }
                  ?>
            </ul>
         </div>
      </div>
   </div>
</div>
