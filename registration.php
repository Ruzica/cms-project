<?php include "includes/db.php"; ?>
<?php include "includes/header.php"; ?>
<?php include "includes/navigation.php"; ?>
<div class="container">
   <div class="col-md-6">
      <div class="text-center">
         <?php if(isset($_GET['errorPassword'])){ ?>
         <div class="alert alert-danger">
            <strong>Lozinke koje ste unijeli nisu iste, molimo Vas pokušajte ponovno!</strong>
         </div>
         <?php } ?>
         <div class="text-center">
            <?php if(isset($_GET['usernameExist'])){ ?>
            <div class="alert alert-danger">
               <strong>Korisničko ime koje ste unijeli već postoji, molimo Vas pokušajte ponovno!</strong>
            </div>
            <?php } ?>
            <h4>Registracija</h4>
         </div>
         <form action="includes/register.php" method="post">
            <div class="form-group">
               <input type="text" name="username" class="form-control" placeholder="Unesite korisničko ime" require>
            </div>
            <div class="form-group">
               <input type="password" name="password" class="form-control" placeholder="Unesite lozinku" require> 
            </div>
            <div class="form-group">
               <input type="password" name="repeat-password" class="form-control" placeholder="Ponovite lozinku" require> 
            </div>
            <div class="text-center">
               <input class="btn btn" type="submit" name="register" value="Registrirajte se">
            </div>
         </form>
      </div>
   </div>
</div>
<br>
<?php if(isset($_GET['error_username_exist'])){ ?>
<div class="alert alert-danger">
   <strong>Username already exist!!!</strong>
</div>
<?php } ?>
<?php if(isset($_GET['register-success'])){ ?>
<div class="alert alert-success">
   <strong>Registration successed!</strong>
</div>
<?php } ?>
</div>
</div>
</div>
<?php include "includes/footer.php"; ?>