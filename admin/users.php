<?php include "includes/admin_header.php";?>
<div id="wrapper">
<?php include "includes/admin_navigation.php";?>
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header">Users</h1>
         <div class="col-xs-6">
            <table class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>
                        <div class='text-center'>Id</div>
                     </th>
                     <th>
                        <div class='text-center'>Username</div>
                     </th>
                  </tr>
               </thead>
               <tbody>
                  <?php readAllUsers();?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<?php include "includes/admin_footer.php";?>