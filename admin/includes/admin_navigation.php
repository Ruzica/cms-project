<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Blog - Administrator</a>
            </div>
        
            <ul class="nav navbar-right top-nav">
            
            <li><a  href="../index.php"><i class="fa fa-home fa-fw"></i>Početna stranica</a></li> 
                <li class="dropdown"> 
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['username']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li>
                            <a href="includes/logout.php"><i class="fa fa-fw fa-power-off"></i>Odjava</a>
                        </li>
                    </ul>
                </li>
            </ul>
               <!-- SIDEBAR IZBORNIK -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="./posts.php"><i class="fa fa-camera-retro fa-lg"></i> All posts</a>
                    </li>

                    <li>
                        <a href="posts.php?source=add_posts"><i class="fa fa-pencil fa-fw"></i> Add new post</a>
                    </li>
                  
                    <li>
                        <a href="categories.php"><i class="fa fa-book fa-fw">Categories</i></a>
                    </li>

                     <li>
                        <a href="users.php"><i class="fa fa-user"></i> Users</a>
                    </li>

                    
                 
                </ul>
            </div>





        </nav>