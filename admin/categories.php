<?php include "includes/admin_header.php";?>
<div id="wrapper">
<?php include "includes/admin_navigation.php";?>
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row">
      <div class="col-lg-12">
         <h1 class="page-header">Categories</h1>
         <div class="col-xs-6">
            <div class="col-xs-6">
               <?php insertCategories(); ?>
               <form action="" method="post">
                  <div class="form-group">
                     <label for="cat-title">Add category</label>
                     <input type="text"  class="form-control" name="cat_title">
                  </div>
                  <div class="form-group">
                     <div class="text-center">
                        <input class="btn btn-primary" type="submit" name="submit" value="Dodaj">
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-xs-6">
               <table class="table table-bordered table-hover">
                  <thead>
                     <tr>
                        <th>Id</th>
                        <th>Title</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php findAllCategories();?>
                     <?php deleteCategory();?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include "includes/admin_footer.php";?>