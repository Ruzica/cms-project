# (CMS) Content Management System
> Simple Blog CMS.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Project Status](#project-status)


## General Information
- The application is based on user login and registration. 
- The user can add new posts, edit and delete old posts.
- There is possibility of adding new posts categories and tags


## Technologies Used
- PHP - version 5.5.9
- HTML - version 5.2
- CSS 
- Bootstrap - version 3.3.6
- MySQL 



## Project Status
Project is: _complete_ 
